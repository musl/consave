# ConSave

This is a fledgeling game framework for [DragonRuby](https://dragonruby.itch.io/dragonruby-gtk).

See also: [DragonRuby Public Source](https://github.com/DragonRuby/dragonruby-game-toolkit-contrib)

## Setup

2. Unzip the dragonruby zip file somewhere.
1. Clone this repo into a directory called `consave` next to the
	 DragonRuby executable.
	
## Running The Thing

1. In the root of the repository run one of the following:
	- Linux / OS X:
 
			$ ./dragonruby consave
 
	- Windows: 
 
			> .\dragonruby.exe consave

