$dragon.require "app/sprite.rb"

class Button
    attr_sprite

    attr_accessor :label, :fg_color, :bg_color, :active_color, :font

    def initialize(opts={})
        @x = opts[:x] || 0
        @y = opts[:y] || 0
        @w = opts[:w] || 100
        @h = opts[:h] || 30
        @label = opts[:label] || ""
        @fg_color = opts[:fg_color] || [0xff, 0xff, 0xff, 0xff]
        @bg_color = opts[:bg_color] || [0x00, 0x00, 0x00, 0xff]
        @active_color = opts[:bg_color] || [0x30, 0x30, 0x30, 0xff]
        @font = opts[:font] || ""
    end

    def draw(args)
        args.borders << [x+1, y+1, w-1, h-1, fg_color]
        # If we can figure out what the font size actually is we can get rid of
        # the magic 12 y offset.
        if args.mouse.point.inside_rect?(self)
            args.solids << [x+1, y+1, w-1, h-1, active_color]
        end
        args.labels << [x+(w/2), y+(h/2)+12, label, 0, 1, fg_color, font]
    end

    def on_click(args, &block)
        click = args.inputs.mouse.click
        block.call if click && click.point.inside_rect?(self)
    end

    def serialize
        {
            x: @x,
            y: @y,
            w: @w,
            h: @h,
            label: @label,
            fg_color: @fg_color,
            bg_color: @bg_color,
            font: @font,
        }
    end

    def inspect
        serialize.to_s
    end

    def to_s
        serialize.to_s
    end

end

class Game
    attr_gtk

    attr_accessor :current_scene

    def initialize(opts = {})
        @current_scene = opts[:current_scene] || 0
    end

    def serialize
        {
            current_scene: self.current_scene
        }
    end

    def inspect
        self.serialize.to_s
    end

    def to_s
        self.serialize.to_s
    end

    def background(color)
        args.solids << [
            args.grid.left + 1,
            args.grid.bottom + 1,
            args.grid.right - 1,
            args.grid.top - 1,
            color
        ]
    end

    def border(width, color)
        args.borders << (1..width).map do |n|
            [
                args.grid.left + n,
                args.grid.bottom + n,
                args.grid.right - 2*n+1,
                args.grid.top - 2*n+1,
                color
            ]
        end
    end

    def framerate
        return unless args.state.show_framerate
        args.labels << [
            14,
            args.grid.top - 15,
            "%0.2f FPS" % [args.gtk.current_framerate],
            -2,
            0,
            0xff,
            0xff,
            0xff,
            0x40,
            "fonts/F25_Bank_Printer_Bold.otf"
       ]
    end

    def tick
        g = args.grid
        #i = args.inputs
        s = args.state
        o = args.outputs

        s.background_color ||= [0x40, 0x40, 0x40, 0xff]
        s.border_color ||= [0x80, 0x80, 0x80, 0xff]
        s.border_size = 4
        s.show_framerate ||= true
        s.current_scene ||= 0

        xo = 6
        yo = 6
        s.save_button ||= Button.new(x: g.left + xo, y: g.bottom + yo, label: "save")
        yo = 38
        s.new_button ||= Button.new(x: g.left + xo, y: g.bottom + yo, label: "new")
        yo = 70
        xo = 108

        #
        # draw stuff
        #
        background(s.background_color)
        border(s.border_size, s.border_color)

        s.new_button.draw(args)
        s.save_button.draw(args)

        # tile area
        o.borders << [g.left+xo+1, g.bottom+7, g.top-13, g.top-13, s.border_color]
        xo += 2 + g.top - 13

        # palette
        #
        o.borders << [g.left+xo+1, g.bottom+7, g.right-xo-7, g.top-13, s.border_color]

        x = g.left+xo + 2
        y = g.top-10
        w = g.right-xo-9
        cols = (w/16).floor
        s.dungeon_sprites ||= Sprite.tile_grid(
            path: "sprites/roguelikeDungeon_transparent.png",
            rows: 18,
            columns: 29,
            tile_w: 16,
            tile_h: 16,
            pad_w: 1,
            pad_h: 1
        ).flatten.each_with_index.map do |sprite, i|
            sprite.x = x+(i%cols)*16
            sprite.y = y-(i/cols)
            sprite.w = 16
            sprite.h = 16
        end

        o.sprites << s.dungeon_sprites

        framerate

        #
        # do stuff
        #
        s.new_button.on_click(args) do
            puts "new button clicked"
        end

        s.new_button.on_click(args) do
            puts "save button clicked"
        end
    end
end

$game = Game.new

def tick(args)
    trace!
    $game.args = args
    $game.tick
end
