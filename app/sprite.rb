class Sprite
	attr_sprite

	attr_accessor :animate, :animation_list, :loop

	def initialize(opts = {})
		@x = opts[:x]
		@y = opts[:y]
		@w = opts[:w]
		@h = opts[:h]

		@angle = opts[:angle]

		@path = opts[:path]

		@a = opts[:a]
		@r = opts[:r]
		@g = opts[:g]
		@b = opts[:b]

		@tile_x = opts[:tile_x]
		@tile_y = opts[:tile_y]
		@tile_w = opts[:tile_w]
		@tile_h = opts[:tile_h]

		@flip_horizontally = opts[:flip_horizontally]
		@flip_vertically = opts[:flip_vertically]

		@angle_anchor_x = opts[:angle_anchor_x]
		@angle_anchor_y = opts[:angle_anchor_y]

		@source_x = opts[:source_x]
		@source_y = opts[:source_y]
		@source_w = opts[:source_w]
		@source_h = opts[:source_h]

		@animation_list = opts[:animation_list] || []
		@animate = opts[:animate]
		@loop = opts[:loop]

		@animation_index = 0
		@animation_tick = 0
	end

    def self.tile_grid(opts = {})
        columns = opts[:columns] || 0
        first_column = opts[:first_column] || 0
        first_row = opts[:first_row] || 0
        offset_x = opts[:offset_x] || 0
        offset_y = opts[:offset_y] || 0
        pad_h = opts[:pad_h] || 0
        pad_w = opts[:pad_w] || 0
        rows = opts[:rows] || 0
        tile_h = opts[:tile_h] || 0
        tile_w = opts[:tile_w] || 0

        sprite_opts = opts[:sprite] || {}
        path = opts[:path] || sprite_opts[:path]

        (0..rows).map do |row|
            (0..columns).map do |column|
                new(
                    sprite_opts.merge({
                        path: path,
                        tile_x: offset_x + (first_row + column) * (tile_w + pad_w),
                        tile_y: offset_y + (first_column + row) * (tile_h + pad_h),
                        tile_w: tile_w,
                        tile_h: tile_h,
                    })
                )
            end
        end
    end

	def self.make_simple_animation_list(tile_x, tile_y, tile_w, tile_h, n, ticks_per_tile, vertical = false, reverse = false)
		list = (0..n).map do |i|
			x = vertical ? 0 : i
			y = vertical ? i : 0
			[tile_x + tile_w * x, tile_y + tile_h * y, ticks_per_tile]
		end

		return reverse ? list.reverse : list
	end

	def set_tile(x, y)
		@tile_x = x * @tile_w
		@tile_y = y * @tile_h
	end

	def tick
		return if @animation_list.empty?
		return unless @animate

		# Animation list is a list of x, y, and ticks-per-tile
		@tile_x, @tile_y, ticks = @animation_list[@animation_index]

		if @animation_tick < ticks
			@animation_tick += 1
			return
		end

		@animation_tick = 0
		@animation_index = (@animation_index + 1) % @animation_list.size
		@animate = @loop || @animation_index != 0
	end

	def serialize
		{
			x: @x,
			y: @y,
			w: @w,
			h: @h,

			angle: @angle,

			path: @path,

			a: @a,
			r: @r,
			g: @g,
			b: @b,

			tile_x: @tile_x,
			tile_y: @tile_y,
			tile_w: @tile_w,
			tile_h: @tile_h,

			flip_horizontally: @flip_horizontally,
			flip_vertically: @flip_vertically,

			angle_anchor_x: @angle_anchor_x,
			angle_anchor_y: @angle_anchor_y,

			source_x: @source_x,
			source_y: @source_y,
			source_w: @source_w,
			source_h: @source_h,

			animate: @animate,
			animation_list: @animation_list,
			loop: @loop,
		}
	end

	def inspect
		serialize.to_s
	end

	def to_s
		serialize.to_s
	end
end
